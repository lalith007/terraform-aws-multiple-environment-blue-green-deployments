# Purpose
This repository is a demo of how I did multiple recreatable environments on AWS with Terraform,
and Blue Green Deployment with ECS.

Huge thanks to this post https://charity.wtf/2016/03/30/terraform-vpc-and-why-you-want-a-tfstate-file-per-env/
It was really helpful

The idea is to have an abstraction on top of Terraform in the form of Makefiles to have multiple environments,
and one statefile per environment, and maintain state in your github repository (This part can be revised by writing more scripts to a remote config management solution, but I wanted to keep that choice open.)



# Makefile key

* Run `make install` to provision your deployment machine


* `make {environment name}-plan`
* `make {environment name}-create`
* `make {environment name}-destroy`
Default Environments - test, qa, staging and prod

* `make temp-create` for creating a temporary environment
* `make temp-plan` for plan for temporary creating an environment
Note down the instance number output at the end of the run, to modify
the temporary environment



* `make INST_ID={INSTANCE_ID} instance-plan`
* `make INST_ID={INSTANCE_ID} instance-create`
* `make INST_ID={INSTANCE_ID} instance-destroy`
Instance Id is the number out from the temp-create run. 


